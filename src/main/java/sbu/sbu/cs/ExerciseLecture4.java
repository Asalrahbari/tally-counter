package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long factor = 1;
        for(int i = 1 ; i <= n ; i++) {
            factor = i * factor;
        }
        long answer = factor;

        return answer;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        if ( n == 1 || n == 2) {
            return  1;
        }
        if ( n > 2) {
            int temp = 0 ;
            int num1 = 1 ;
            int num2 = 1 ;
            for ( int j = 1 ; j <= n ; j++) {
                temp = num1 + num2 ; 
                num1 = num2;
                num2 = temp;
                
            }
            return temp;
        }
        return 1;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder name = new StringBuilder(word);
        return name.reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String str = line.replaceAll(" ", "");
        String b = reverse(str);
        boolean value = str.equalsIgnoreCase(b);
        return value;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
         char[][] arr = new char[str1.length()][str2.length()];

        for (int i = 0; i < str1.length(); i++) 
        {
            for (int j = 0; j < str2.length(); j++)
             {
                if ( str1.charAt(i) == str2.charAt(j) )
                    arr[i][j] = '*';
                else 
                {
                    arr[i][j] = ' ';
                }
            }
        }
        return arr;
    }
}
