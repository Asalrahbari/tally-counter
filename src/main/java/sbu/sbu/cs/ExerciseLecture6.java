package sbu.cs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        int number = 0 ;
        for ( int i = 0 ; i < arr.length; i++) {
            if ( i % 2 == 0) {
                number += arr[i];
            }
            
        }
        return number;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reverseofarr = new int[arr.length];
        int lengthh = arr.length;
          for ( int i = 0 ; i < arr.length; i++) {
              reverseofarr[i] = arr[lengthh];
              lengthh = lengthh -1 ;
        }
        return reverseofarr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        double[][] ar = new double[m1.length][m2[0].length];
         if (m1.length == m2[0].length) {
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m1.length; j++) {
                    for (int t = 0; t < m2[0].length; t++) {
                        for (int p = 0; p < m2[0].length; p++) {
                            ar[i][t] += (m1[i][j] * m2[p][t]);
                        }
                    }
                }
            }
        }
        if ( m1.length != m2[0].length )
            throw new RuntimeException("incorrect");

        return ar;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
          List<List<String>> listt = new ArrayList<>();
        int length4 = names.length;
        for (int i = 0; i < length4 ; i++)
        {
            List<String> list1= new ArrayList<>();
            int x = names[i].length;
            for (int j = 0; j < x; j++)
            {
                list1.add(names[i][j]);
            }
            listt.add(list1);
        }
        return listt;
        }

    

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> list2 = new ArrayList<>();
        for (int i = 2; i <= n; i++)
        {
            if(n % i == 0 && primenumber(i) == true)
            {
                list2.add(i);
            }
        }
        Collections.sort(list2);
        return list2;

    }
    public boolean primenumber ( int y ) {
        if ( y <= 1 ) 
        return false;
        for ( int u = 2 ; u < y ; u++) {
            if ( y % u == 0)
            return false;
        }
        return true;
    }


    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] words = line.split(" ");
        List<String> list3 = new ArrayList<>();
        for (String A : words)  {
            String ADD = A.replaceAll("[^a-zA-Z]","");
            list3.add(ADD);
        }
        return list3;
    }
    
}
