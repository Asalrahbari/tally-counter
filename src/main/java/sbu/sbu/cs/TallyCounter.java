package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    
    private int counter;
    public void count() {
        if ( counter < 9999) {
            counter = counter + 1 ;
        }

    }

    
    public int getValue() {
        return counter;
    }
    
    public void setValue(int value) throws IllegalValueException {
        if ( value < 0 || value > 9999) {
            throw new IllegalValueException();
        }
        counter = value;

    }
}
