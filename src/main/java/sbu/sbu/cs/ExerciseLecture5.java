package sbu.cs;
import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
         Random charc = new Random();
        char[] pasword = new char[length];
        String strr = "abcdefghijklmnopqrstuvwxyz";
        for ( int i = 0 ; i < length ; i++) {
            int k = charc.nextInt(strr.length());
            pasword[i] = strr.charAt(k);
        }
        return pasword.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        Random rnd = new Random();
        char[] password = new char[length];
        String str1 = "abcdefghijklmnopqrstuvwxyz";
        String str2 = "1234567890";
        String str3 = "@!%$&";
        String str4 = str1 + str2 + str3;
        int number1 = rnd.nextInt(str1.length());
        int number2 = rnd.nextInt(str2.length());
        int number3 = rnd.nextInt(str3.length());
        password[0] = str1.charAt(number1);
        password[1] = str2.charAt(number2);
        password[2] = str3.charAt(number3);
        for (int j = 3; j < length; j++) {
            int number4 = rnd.nextInt(str4.length());
            password[j] = str4.charAt(number4);


        }
        return password.toString();
    }

        /*
         *   implement a function that checks if a integer is a fibobin number
         *   integer n is fibobin is there exist an i where:
         *       n = fib(i) + bin(fib(i))
         *   where fib(i) is the ith fibonacci number and bin(i) is the number
         *   of ones in binary format
         *   lecture 5 page 17
         */
        public int fibonacci(int n) {
            int num1 = 1, num2 = 1, num = 0;
            if (n == 1 || n == 2) {
                return 1;
            }
            if (n > 2)
            {
                for(int i = 1;i <=n ; i++)
                {
                    num = num1 + num2;
                    num1 = num2;
                    num2 = num;
                }
                return num;
            }
            return 1;
        }
    public boolean isFiboBin(int n) {
        for (int i = 0; i < n; i++) {
            int fib1 = fibonacci(i);
            String b = Integer.toBinaryString(fib1);
            int s = 0;
            for (int j = 0; j < b.length(); j++) {
                if (b.charAt(j) == '1')
                    s++;
            }
            int a = fib1 + s;
            if (a == n)
                return true;
        }
        return false;
    }
}